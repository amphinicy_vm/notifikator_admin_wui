'use strict';

angular.module('NotifikatorAdmin').factory('loginInterceptor', ['$q', '$rootScope', 'Auth', function($q, $rootScope, Auth) {  
	var requestInterceptor = {
		request: function(config) {
			if ( !Auth.isLoggedIn() ) {
				$rootScope.$emit("unauthorized");
			}
			return config;
		}
	};
	return requestInterceptor;
}]);