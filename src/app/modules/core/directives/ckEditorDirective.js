'use strict';

angular.module('core')
	.directive('ckEditor', ['$rootScope', function ($rootScope) {
		return {
			require: '?ngModel',
			link: function (scope, elm, attr, ngModel) {
				CKEDITOR.config.contentsCss = '/ckeditor/ckeditor-overrides.css';

				var options = {
					height:500,
					toolbar:[{ name: 'document', items: [ 'Bold','-','Undo','Redo','-','Image' ] },{ name: 'links', items: [ 'Link', 'Unlink' ] }]
				};
				var ck = CKEDITOR.replace(elm[0], options);

				if (!ngModel) { 
					return; 
				}
				function updateModel () {
					scope.$apply(function () {
						ngModel.$setViewValue(ck.getData());
					});
				}
				ck.on('change', updateModel);
				ck.on('key', updateModel);
				ck.on('dataReady', updateModel);

				ngModel.$render = function (value) {
					ck.setData($rootScope[attr.ngModel]);
				};

				$rootScope.$on('refresh', function() {
					console.log('Refresh cheditors ');
					var newData = $rootScope[attr.ngModel];
				});
			}
		};
	}]);