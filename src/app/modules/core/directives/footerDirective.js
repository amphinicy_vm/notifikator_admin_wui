'use strict';

angular.module('core')
	.directive('notifikatorFooter', function() {
		return {
			replace: true,
			restrict: 'E',  
			templateUrl: 'app/modules/core/templates/directive/footer.html',
			controller: 'footerCtrl'
		};
	});