'use strict';

angular.module('core')
	.controller('headerCtrl', ['$scope', 'socketIoFactory', 'Auth', function ($scope, socketIoFactory, Auth) {
		socketIoFactory.getAllClients();

		$scope.logMeOut = function(){
			Auth.logout();
		};

		$scope.refreshMe = function(){
			socketIoFactory.refreshMe();
		};

		$(function () {
		  $('[data-toggle="tooltip"]').tooltip();
		});
	}]);