'use strict';

angular.module('NotifikatorAdmin').factory('socketIoFactory', ['$rootScope', '$timeout', 'Auth', function ($rootScope, $timeout, Auth) {
	$rootScope.initializingSocket = true;

	var appData = {
		clients: [],
		date: ''
	};

	var socket = io.connect($rootScope.socket_address);
	
	socket.on('connect', function (data) {
		delete $rootScope.socketError;
		$rootScope.initializingSocket = false;
		$('#connection-error').modal('hide');
	});

	socket.on('disconnect', function () {
		delete $rootScope.clientsRS;
		$rootScope.initializingSocket = true;
		$rootScope.socketError = "Disconnected!";
		$rootScope.$apply();
	});

	// if connection fails for some reason
	socket.on('connect_failed', function (data) {
		$rootScope.initializingSocket = true;
		$rootScope.socketError = "Connection to server failed! <br><br><span class='text-danger'>" + data + "</span>";
		$rootScope.$apply();
		$('#connection-error').modal('show');
	});

	// if server is offline
	socket.on('connect_error', function (data) {
		$rootScope.initializingSocket = true;
		$rootScope.socketError = "Error connecting to server! <br><br><span class='text-danger'>" + data + "</span>";
		$rootScope.$apply();
		$('#connection-error').modal('show');
	});

	socket.on('loginfailed', function (message) {
		$rootScope.loginError = message;
		$rootScope.$apply();
	});

	socket.on('messages', function (data){
		if( data.clients ) {
			$rootScope.clientsRS = data.clients;
			$rootScope.$apply();
		} else if ( data.settings ) {
			if ( data.settings.dailynews ) $rootScope.dailyNews = data.settings.dailynews;
			if ( data.settings.weekmenu ) $rootScope.weekMenu = data.settings.weekmenu;
			if ( data.settings.money ) {
				$rootScope.money = data.settings.money;
				$('#close-month').modal('hide');
				$rootScope.closingMonth = 0;
			}
			$rootScope.$apply();
			$rootScope.$broadcast("refresh");	// Refresh ckeditors
		}
	});

	socket.on('lunchgroups', function (data) {
		$rootScope.lunchGroups = data;
	});

	socket.on('randstr', function (randstr) {
		Auth.setRandStr(randstr);
		$rootScope.$apply();
	});

	socket.on('notification', function (data) {
		if( data.type === "success" ) toastr.success(data.message, data.title);
		if( data.type === "error" ) toastr.error(data.message, data.title);
		if( data.type === "warning" ) toastr.warning(data.message, data.title);
	});

	socket.on('login', function (state) {
		if( state ) {
			delete $rootScope.loginError;
			Auth.setUser('Admin');
		}
	});

	appData.sendNotification = function (NotifikatorData) {
		socket.emit('messages', {
			title: NotifikatorData.title, 
			message: NotifikatorData.message, 
			image: NotifikatorData.image, 
			recepients: NotifikatorData.clients, 
			from: 'admin'
		});
	}

	appData.callLunchGroup = function (title, message, image, recepients) {
		socket.emit('messages', {
			title: title, 
			message: message, 
			image: image, 
			recepients: recepients, 
			from: 'visnja', 
			grupa: title
		});
	}

	appData.giveMeTheMoney = function (title, message, image, recepients) {
		socket.emit('messages', {
			title: title, 
			message: message, 
			image: image, 
			recepients: recepients, 
			from: 'moneyman'
		});	
	}

	appData.saveDailyNews = function (data) {
		socket.emit('settings', {dailynews: {content: data}});
	}

	appData.saveWeekMenu = function (data) {
		socket.emit('settings', {weekmenu: {content: data}});
	}

	appData.refreshMe = function (data) {
		socket.emit('refreshMe');
	}

	appData.getAllClients = function () {
		var clients;
		socket.emit('getAllClients', '', function (data) {
			appData.clients = data;
		});
	}

	appData.payForLunch = function (name, status, month) {
		socket.emit('payforlunch', {
			name: name, 
			status: status, 
			month: month
		});
	}

	appData.loadMonth = function (id) {
		socket.emit('loadmonth', {id: id});
	}

	appData.closeMonth = function (id) {
		socket.emit('closemonth', {id: id}, function (data) {
			$rootScope.closingMonth = 0;
		});
	}

	appData.initiateAuth = function (hash) {
		if(hash === undefined) {
			socket.emit('join', 'Admin');
		} else {
			socket.emit('join', {name: 'Admin', hash: hash});
		}
	}

	appData.editGroup = function (name, from, to, position, date) {
		socket.emit('editGroup', {name: name, from: from, to: to, position: position, date: date});
	}

	return appData;
}]);