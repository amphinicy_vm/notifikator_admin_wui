'use strict';


angular.module( 'core', [
        'ngResource',
        'ngCookies',
        'ngSanitize',
        'ui.router',
    ] )
    .config( ['$stateProvider', function ( $stateProvider ) {
        // No states in this module
    }] );

