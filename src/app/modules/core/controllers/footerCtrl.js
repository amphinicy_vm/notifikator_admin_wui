'use strict';

angular.module('core')
	.controller('footerCtrl', ['$scope', function ($scope) {
		$scope.time = new Date();
	}]);