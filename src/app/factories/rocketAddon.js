'use strict';

// Rocket integration documentation - https://rocket.chat/docs/administrator-guides/integrations/

angular.module('NotifikatorAdmin').factory('RocketAddon', ['$http', function ($http) {
	var url = "https://chat.amphinicy.com/hooks/KrLxnZXuqBeygvB6F/YAd3D77hP8ws2PTrxqZhN6DHNNgXMxHLKFQhfPqwwpidNfhg";

	return {
		// used to send custom message to Rocket chat. This can be sent from admin by selecting RocketBot as target user
		sendMessage : function (title, message, image) {
			$http.post(url, {"attachments": [
								{
									"author_name": "Admin",
									"author_icon": "http://rucak.amphinicy.com/assets/images/icon.png",
									"title": title,
									"text": message,
									"image_url": image,
									"color": "#c4da60"
								}
							]})
				.then(function(response) {
					if (response.data.success === true) {
						toastr.success('Rocket message delivered!"', 'RocketBot');
					} else {
						toastr.error('Rocket message NOT delivered!"', 'RocketBot');
					}
				});
		},
		groupStart : function (group, image) {
			$http.post(url, {"attachments": [
								{
									"author_name": "Teta Višnja",
									"author_icon": "http://rucak.amphinicy.com/assets/images/teta_visnja.png",
									"title": group,
									"text": "Ručak je na stolu",
									"image_url": image,
									"color": "#c4da60"
								}
							]})
				.then(function(response) {
					if (response.data.success === true) {
						toastr.success('Rocket message delivered!"', 'RocketBot');
					} else {
						toastr.error('Rocket message NOT delivered!"', 'RocketBot');
					}
				});
		}
	}
}]);