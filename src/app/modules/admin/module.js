'use strict';

angular.module( 'admin', [
		'ngResource',
		'ngCookies',
		'ngSanitize',
		'ui.router', 
		'ui.select'
	] )
	.config( ['$stateProvider', function ( $stateProvider ) {
		// state has to have prefix of the module name (because of possible override with other states)
		$stateProvider
			.state( 'admin', {
				url: '/admin',
				templateUrl: 'app/modules/admin/templates/admin.html',
				controller: 'adminCtrl'
			})
			.state('admin.lunch', {
				url : '/lunch',
				templateUrl : 'app/modules/admin/templates/admin/rucak.html',
				controller : 'adminRucakCtrl'
			})
			.state('admin.notifications', {
				url : '/notifications',
				templateUrl : 'app/modules/admin/templates/admin/notifications.html',
				controller : 'adminNotificationsCtrl',
				params : { notify: null }
			})
			.state('admin.manage', {
				url : '/manage',
				templateUrl : 'app/modules/admin/templates/admin/manage.html',
				controller : 'adminManageCtrl'
			})
			.state('admin.settings', {
				url : '/settings',
				templateUrl : 'app/modules/admin/templates/admin/settings.html',
				controller : 'adminSettingsCtrl'
			});
	}] );

