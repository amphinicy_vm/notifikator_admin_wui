'use strict';

angular.module('admin').controller('adminCtrl', ['$scope', 'NotifikatorDataFactory', function ($scope, NotifikatorDataFactory) {
	$scope.NotifikatorData = NotifikatorDataFactory;

	$scope.testNotifications = function() {
		if(window.Notification && Notification.permission !== "denied") {
			Notification.requestPermission(function(status) {
				new Notification($scope.NotifikatorData.title, { 
					tag: 'test',
					body: $scope.NotifikatorData.message,
					icon: 'assets/images/icon.png',
					image: $scope.NotifikatorData.image
				}); 
			});
		}
	};

	$scope.exampleNotification = function() {
		if(window.Notification && Notification.permission !== "denied") {
			Notification.requestPermission(function(status) {
				new Notification('imageExample', { 
					tag: 'exampleimage',
					body: 'This is how image notification looks like!',
					icon: 'assets/images/icon.png',
					image: 'https://unsplash.it/300/200/?random'
				});
			});
			Notification.requestPermission(function(status) {
				new Notification('textExample', {
					tag: 'exampleTxt',
					body: 'This is how text notifications looks like!',
					icon: 'assets/images/icon.png'
				});
			});
		}
	};
}]);
