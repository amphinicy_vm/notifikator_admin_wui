'use strict';

angular.module('admin')
	.controller('adminSettingsCtrl', ['$rootScope', '$scope', 'socketIoFactory', function ($rootScope, $scope, socketIoFactory) {
		$scope.activeCkEditor = 'cknews';

		$scope.saveDailyNews = function() {
			socketIoFactory.saveDailyNews($scope.dailyNews);
		};

		$scope.saveWeekMenu = function() {
			socketIoFactory.saveWeekMenu($scope.weekMenu);
		};

		$scope.pay = function($event) {
			var name = angular.element($event.currentTarget).parent().find("span").text();
			var status = 1;
			if ( angular.element($event.currentTarget).hasClass('btn-danger') ) {
				status = 0;
			}
			socketIoFactory.payForLunch(name, status, $rootScope.money.selected.month);
		};

		$scope.isOnline = function (name) {
			var usernames = _.map($rootScope.clientsRS, 'name');
			if ( _.includes(usernames, name) ) return true;
			else return false;
		}

		$scope.giveMeTheMoney = function() {
			var toNotify = [];
			_.each($rootScope.money.selected.data, function(value, key){
				if($scope.isOnline(key) && value === 0) {
					toNotify.push(key);
				}
			});
			socketIoFactory.giveMeTheMoney('Još nisi da(o/la) lovu za tekući mjesec!', 'Znam gdje sjediš i gdje se krećeš!', 'images/lunchMoney.jpg', toNotify);
		}

		$scope.loadMonthPayments = function($event){
			var element = angular.element($event.currentTarget);
			socketIoFactory.loadMonth(element[0].innerText);
		}

		$scope.closeMonthPayments = function(){
			$rootScope.closingMonth = 1;
			socketIoFactory.closeMonth($rootScope.money.selected.month);
		}

		$scope.addImage = function(url, editor) {
			if ( url ) {
				CKEDITOR.instances[editor].insertHtml( '<img src="' + url + '" alt="image" />' );
			}
		}

		$scope.addYT = function(url, editor) {
			var yt = url.match(/v=(\w+)/);
			console.log(yt[1]);
			if ( url ) CKEDITOR.instances[editor].insertHtml( '<img youtube-placeholder src="../assets/images/youtube-bar.png" style="height: 20px;"/>' + '<br>' + '<img youtube src="https://img.youtube.com/vi/' + yt[1] + '/hqdefault.jpg" alt="youtube ' + yt[1] + '"/>' );
		}

		$scope.changeActiveCkedit = function(activeCkEditor) {
			$scope.activeCkEditor = activeCkEditor;
		}

		$scope.legendToggle = function() {
			$("#collapse-legend").collapse('toggle');
		}

		$(function () {
			$('[data-tooltip="tooltip"]').tooltip();
		})
	}]);