'use strict';

angular.module('core')
	.controller('galleryCtrl', ['$rootScope', '$scope', 'socketIoFactory', 'NotifikatorDataFactory', '$http', function ($rootScope, $scope, socketIoFactory, NotifikatorDataFactory, $http) {
		$scope.getMoreImages = function(){
			$http.get('http://'+$rootScope.images_manager+'/gallery?offset=' + $rootScope.gallery.length)
					.then(function(response) {
						$rootScope.gallery = _.union($rootScope.gallery, response.data.files);
						$scope.totalImages = response.data.count;
						if( $rootScope.gallery.length === response.data.count ) {
							$scope.noMoreImages = true;
						}
					});
		};

		$scope.useMe = function($event) {
			var element = angular.element($event.currentTarget);
			if($scope.updateElement) {
				$($scope.updateElement).val('http://' + $rootScope.images_host + '/' + element.attr('file-name'));
				$($scope.updateElement).trigger('change');
			} else if ($scope.activeCkEditor) {
				CKEDITOR.instances[$scope.activeCkEditor].insertHtml( '<img src="http://' + $rootScope.images_host + '/' + element.attr('file-name') + '" />' );
			} else {
				NotifikatorDataFactory.image = 'http://' + $rootScope.images_host + '/' + element.attr('file-name');
			}
			$('#galerija').modal('hide');
		};

		$scope.uploadFile = function() {
			$('#upload-input').click();
			$('.progress-bar').text('0%');
			$('.progress-bar').width('0%');
		};
	}]);