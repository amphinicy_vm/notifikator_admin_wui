'use strict';

angular.module('core')
	.directive('notificatorGallery', ['$http', '$rootScope', function ($http, $rootScope) {
		return {
			replace: true,
						restrict: 'E',  
						templateUrl: 'app/modules/core/templates/directive/gallery.html',
						controller: 'galleryCtrl',
			link: function (scope, elm, attr, ngModel, rootScope) {
				$(elm).on('show.bs.modal', function (e) {
					$http.get("http://"+$rootScope.images_manager+"/gallery")
							.then(function(response) {
									$rootScope.gallery = response.data.files;
									scope.totalImages = response.data.count;
									if( $rootScope.gallery.length === response.data.count ) {
										scope.noMoreImages = true;
									}
							});

				});

				// Used when we are changing attribute value. Useful when working with input fields.
				scope.updateElement = attr.updateElement;
				// Used when we need to change a complex value like when using ckeditor.
				scope.updateCkeditor = attr.updateCkeditor;

				$('#upload-input').on('change', function(){
					var files = $(this).get(0).files;

					if (files.length > 0){
						var formData = new FormData();

						// loop through all the selected files
						for (var i = 0; i < files.length; i++) {
							var file = files[i];

							// Check if the file is an image
							if( file.type.indexOf('image') != -1 ){
								// add the files to formData object for the data payload
								formData.append('uploads[]', file, file.name);
							}
						}

						$.ajax({
							url: 'http://'+$rootScope.images_manager+'/upload',
							type: 'POST',
							crossDomain: true, 
							data: formData,
							processData: false,
							contentType: false,
							success: function(imgs){
									imgs = JSON.parse(imgs);
									for (var i = imgs.length - 1; i >= 0; i--) {
										$rootScope.gallery.unshift(imgs[i]);
									}
									$rootScope.$apply();
									toastr.success(imgs.length + ' new images uploaded!', 'Gallery')
							},
							xhr: function() {
								var xhr = new XMLHttpRequest();

								xhr.upload.addEventListener('progress', function(evt) {

									if (evt.lengthComputable) {
										var percentComplete = evt.loaded / evt.total;
										percentComplete = parseInt(percentComplete * 100);

										// update the Bootstrap progress bar with the new percentage
										$('.progress-bar').text('');
										$('.progress-bar').width(percentComplete + '%');

										// once the upload reaches 100%, set the progress bar text to done
										if (percentComplete === 100) {
											$('.progress-bar').html('');
										}

									}

								}, false);

								return xhr;
							}
						});
					}

				});

				$('#galerija').on('click', '.delete', function(){
					var span = $(this).parent().find("span.gallery-item");
					var filename = span.attr('file-name');
					$.ajax({
							url: 'http://'+$rootScope.images_manager+'/gallery/' + filename,
							type: 'DELETE',
							crossDomain: true,
							dataType: 'json', 
							data: {},
							success: function(data){
								if ( data.deleted ){
									scope.totalImages = data.count;
									_.remove($rootScope.gallery, function(image){
											return ( filename === image );
									});
									$rootScope.$apply();
									toastr.success('Image deleted!', 'Gallery')
								}
							}
						});
				});
			}
		};
	}]);