'use strict';

angular.module('NotifikatorAdmin').factory('Auth', ['localStorageService', function (localStorageService) {
	var user, 
		randStr, 
		hash = localStorageService.get('hash');

	return {
		setUser : function (name) {
			user = name;
		},
		getUser : function () {
			return user;
		},
		logout : function () {
			localStorageService.clearAll();
			user = false;
			hash = undefined;
		},
		setRandStr : function (rStr) {
			randStr = rStr;
		},
		getRandStr : function () {
			return randStr;
		},
		isLoggedIn : function () {
			return (user) ? user : false;
		},
		setHash : function (h) {
			hash = h;
			localStorageService.set('hash', h);
		},
		getHash : function () {
			return hash;
		}
	}
}]);