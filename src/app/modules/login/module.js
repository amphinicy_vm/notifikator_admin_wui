'use strict';

angular.module( 'login', [
        'ngResource',
        'ngCookies',
        'ngSanitize',
        'ui.router',
    ] )
    .config( ['$stateProvider', function ( $stateProvider ) {
        // state has to have prefix of the module name (because of possible override with other states)
        $stateProvider
            .state( 'login', {
                url: '/login',
                templateUrl: 'app/modules/login/templates/login.html',
                controller: 'loginCtrl'
            } );
    }] );

