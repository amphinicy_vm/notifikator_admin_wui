'use strict';

angular.module('NotifikatorAdmin', [
	'ngResource', 
	'ngCookies', 
	'ngSanitize',
	'ui.router', 
	'ui.bootstrap', 
	'LocalStorageModule',
	// list your modules here
	'core',
	'login',
	'admin'
])
	
.config(['$locationProvider', '$urlRouterProvider', '$httpProvider', function ($locationProvider, $urlRouterProvider, $httpProvider) {
	$urlRouterProvider.otherwise('/admin');
	$locationProvider.html5Mode(true);
	$httpProvider.interceptors.push('loginInterceptor');
}])

.config(['localStorageServiceProvider', function(localStorageServiceProvider){
	localStorageServiceProvider.setPrefix('notifikator');
}])

.controller('mainCtrl', ['$rootScope', '$scope', 'socketIoFactory', 'Auth', '$state', function ($rootScope, $scope, socketIoFactory, Auth, $state) {
	$rootScope.loggedin = Auth.isLoggedIn();

	$scope.$watch(function(){
		return Auth.isLoggedIn();
	}, function(newValue, oldValue){
		$rootScope.loggedin = Auth.isLoggedIn();
		if(newValue != false) $state.go('admin');
		else $state.go('login');
	});
}])

.run(['$rootScope', '$state', '$stateParams',
	function ($rootScope, $state, $stateParams) {
		$rootScope.$state = $state;
		$rootScope.$stateParams = $stateParams;
		$rootScope.$on('unauthorized', function () {
			$state.transitionTo('login');
		});

		// loading resources config on app start
		for ( var key in resources ) {
			if ( !resources.hasOwnProperty(key) ) continue;
			$rootScope[key] = resources[key];
		}
	}]
);