'use strict';

// When login button is clicked run the authentication process
// 1. send username "admin" as intention admin will be joining
// 2. receive rand string
// 3. send hash(pass + randString) and store hash to Auth factory
// 4. receive status of login process
// 5. if everything is ok set cookie with the hash from step 3. This hash will be used for requests

angular.module('login').controller('loginCtrl', ['$rootScope', '$scope', 'socketIoFactory', 'Auth', function ($rootScope, $scope, socketIoFactory, Auth) {
	$scope.time = new Date();
	$scope.username = Auth.getUser();

	if ( !$scope.username && Auth.getHash() ){
		socketIoFactory.initiateAuth(Auth.getHash().toString(CryptoJS.enc.Hex));
	}
	
	$scope.logIn = function() {
		socketIoFactory.initiateAuth();
	};

	$scope.$watch(function(){
		return Auth.getRandStr();
	}, function(newValue, oldValue){
		if ( newValue === oldValue ) {
			return;
		}
		var hashMe = $scope.pass + Auth.getRandStr();
		var hash = CryptoJS.SHA256(hashMe);
		Auth.setHash(hash.toString(CryptoJS.enc.Hex));
		socketIoFactory.initiateAuth(hash.toString(CryptoJS.enc.Hex));
	});
}]);