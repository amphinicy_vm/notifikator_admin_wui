'use strict';

angular.module('NotifikatorAdmin').factory('NotifikatorDataFactory', function () {
	return {
		title : '',
		message: '',
		clients: []
	};
});