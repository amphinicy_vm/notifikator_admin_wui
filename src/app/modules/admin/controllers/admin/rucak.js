'use strict';

angular.module('admin').controller('adminRucakCtrl', ['$rootScope', '$scope', 'NotifikatorDataFactory', 'socketIoFactory', '$state', 'RocketAddon', function ($rootScope, $scope, NotifikatorDataFactory, socketIoFactory, $state, RocketAddon) {
    $scope.name = 'Lunch view';

    var today = new Date();
    $scope.today = today.yyyy_mm_dd() + ',' + today.getDayName();

    $scope.selectedDate = $scope.today;

    $scope.NotifikatorData = NotifikatorDataFactory;

    $scope.groupClass = 'col-xs-12 col-md-6';
    if ( $rootScope.lunchGroups && Object.keys($rootScope.lunchGroups).length > 2 ){
    	$scope.groupClass = 'col-xs-12 col-md-4';
    }

    $scope.callLunchGroup = function(group) {
        socketIoFactory.callLunchGroup(group, 'Lunch is on the table!', $scope.lunchImage, $scope.lunchGroups[group]);
        toastr.success('Called group - "' + group + '"', 'Lunch');
        RocketAddon.groupStart(group, $scope.lunchImage);
    };

    $scope.changeSref = function (name) {
        if ( this.isOnline(name) ) $state.go('admin.notifications', {notify: name});
    }

    $scope.isOnline = function (name) {
        var usernames = _.map($rootScope.clientsRS, 'name');
        if ( _.includes(usernames, name) ) return true;
        else return false;
    }

    $scope.notifyUser = function ($event) {
        var element = angular.element($event.currentTarget);

        if( $scope.NotifikatorData.clients.indexOf(element.text()) === -1 ) {
            $scope.NotifikatorData.clients.push(element.text());
        }
    }

    $scope.selectLunchDate = function ($event) {
        if ( $event.currentTarget.textContent === "Today" ) {
            $scope.selectedDate = $scope.today;
        } else {
            $scope.selectedDate = $event.currentTarget.textContent;
        }
    }

    function doubleDigit (number) {
    	if (number < 10) {
    		return ("0" + number);
    	}
    	return number;
    }

    $( function() {
        $( ".connectedSortable" ).sortable({
            connectWith: ".connectedSortable",
            placeholder: "sortable-placeholder",
            receive: function( event, ui ) {
                var from = $(ui.sender).parent().find(".panel-heading").text().trim();
                var to = $(event.target).parent().find(".panel-heading").text().trim();
                var name = $(event.originalEvent.target).text().trim();
                var position = ui.item.index();
                toastr.success(from + " --> " + to, name);
                socketIoFactory.editGroup(name, from, to, position, $scope.selectedDate);
            },
            remove: function( event, ui ) {}
        }).disableSelection();
    });
}]);
