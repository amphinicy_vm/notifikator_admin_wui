'use strict';

angular.module('NotifikatorAdmin')
	.directive('notifikatorHeader', function() {
		return {
			replace: true,
			restrict: 'E',  
			templateUrl: 'app/modules/core/templates/directive/header.html',
			controller: 'headerCtrl'
		};
	});