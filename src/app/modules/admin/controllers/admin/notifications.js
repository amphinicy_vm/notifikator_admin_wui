angular.module('admin').controller('adminNotificationsCtrl', ['$rootScope', '$scope', 'NotifikatorDataFactory', 'socketIoFactory', 'RocketAddon', function ($rootScope, $scope, NotifikatorDataFactory, socketIoFactory, RocketAddon) {
    'use strict';
    $scope.name = 'Notification generator';

    $scope.NotifikatorData = NotifikatorDataFactory;

    // Triggered when user is selected on another page and redirected to notifications.
    $scope.$on('modifyPrimatelji', function(ngRepeatFinishedEvent) {
        $scope.NotifikatorData.clients = [];
        if ( $scope.$stateParams.notify ) {
            if( $scope.NotifikatorData.clients.indexOf($scope.$stateParams.notify) === -1 ) {
                $scope.NotifikatorData.clients.push($scope.$stateParams.notify);
            }
            $("a[data-clients]").each(function(){
                if ( $(this).text() === $scope.$stateParams.notify ) {
                    $(this).addClass('list-group-item-success');
                }
            });
        }
    });

    $scope.sendNotifcation = function() {
        socketIoFactory.sendNotification($scope.NotifikatorData);
        toastr.success('Notification sent!"', 'Notificator');
        if ( _.indexOf($scope.NotifikatorData.clients, 'RocketBot') >= 0 ) {
            RocketAddon.sendMessage($scope.NotifikatorData.title, $scope.NotifikatorData.message, $scope.NotifikatorData.image);
        }
    };

    $scope.selectUser = function($event) {
        var element = angular.element($event.currentTarget);
        if ( element.hasClass('list-group-item-success') ) {
            element.removeClass('list-group-item-success');
            _.pull($scope.NotifikatorData.clients, element.text());
        } else {
            element.addClass('list-group-item-success');
            if( $scope.NotifikatorData.clients.indexOf(element.text()) === -1 ) {
                $scope.NotifikatorData.clients.push(element.text());
            }
        }
    };

    $scope.selectAll = function() {
        $("a[data-clients]").each(function(){
            $(this).addClass('list-group-item-success');
            if( $scope.NotifikatorData.clients.indexOf($(this).text()) === -1 ) {
                $scope.NotifikatorData.clients.push($(this).text());
            }
        });
    };

    $scope.removeAll = function() {
        $("a[data-clients]").each(function(){
            $(this).removeClass('list-group-item-success');
        });
        $scope.NotifikatorData.clients = [];
    };
}]);
